Implementation Reference
========================
This is the developer's reference for the underlying implementation of Jygsaw.
Jygsaw is built on Jython, which in turn uses the Java Virtual Machine. Many
features of Jygsaw are actually wrapped
`Java AWT <http://docs.oracle.com/javase/7/docs/api/java/awt/package-summary.html>`_
and
`Swing <http://docs.oracle.com/javase/7/docs/api/javax/swing/package-summary.html>`_
classes and methods.

.. toctree::
    :maxdepth: 1

    graphicswindow
    canvas
    graphicsobject
    group
    image
    shape
    text
