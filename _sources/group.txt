Group Class
===========

.. currentmodule:: jygsaw.group

Method Summaries
----------------
.. autosummary::
    Group
    Group.append
    Group.remove
    Group.move

.. autoclass:: Group
    :members:
    :undoc-members:
