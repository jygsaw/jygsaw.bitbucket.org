Canvas Class
============

.. currentmodule:: jygsaw.graphicswindow

External Method Summaries
-------------------------
.. autosummary::
    Canvas
    Canvas.blocking_redraw

Attribute Summaries
-------------------
.. autosummary::
    Canvas.background_color
    Canvas.filled
    Canvas.default_color
    Canvas.stroke
    Canvas.stroke_color
    Canvas.stroke_width
    Canvas.font
    Canvas.text_size

Internal Method Summaries
-------------------------
.. autosummary::
    Canvas.paintComponent

Details
-------
.. autoclass:: Canvas
    :members:
    :undoc-members:
